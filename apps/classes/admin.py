from django.contrib import admin
from .models import Class, PowerSource, ClassBenefit

for model in (Class, PowerSource, ClassBenefit):
    admin.site.register(model)

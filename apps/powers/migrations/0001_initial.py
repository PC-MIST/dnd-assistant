# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-15 21:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('classes', '0001_initial'),
        ('character', '0002_auto_20171115_2135'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActionType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=80, verbose_name='Тип действия')),
                ('value', models.SmallIntegerField(help_text='Для таких типов как Ближняя вспышка, Ближняя волна, Зональная стена', verbose_name='Значение')),
            ],
            options={
                'verbose_name_plural': 'Типы действий',
                'verbose_name': 'Тип действия',
                'db_table': 'Action_types',
            },
        ),
        migrations.CreateModel(
            name='AttackType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=80, verbose_name='Тип атаки')),
            ],
            options={
                'verbose_name_plural': 'Типы атак',
                'verbose_name': 'Тип атаки',
                'db_table': 'Attack_types',
            },
        ),
        migrations.CreateModel(
            name='PowerAccessory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=80, verbose_name='Тип приспособления')),
            ],
            options={
                'verbose_name_plural': 'Приспособления для использования таланта',
                'verbose_name': 'Приспособление для использования таланта',
                'db_table': 'Power_accessories',
            },
        ),
        migrations.CreateModel(
            name='PowerDamageType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=80, verbose_name='Тип урона')),
            ],
            options={
                'verbose_name_plural': 'Виды урона',
                'verbose_name': 'Вид урона',
                'db_table': 'Power_damage_types',
            },
        ),
        migrations.CreateModel(
            name='PowerEffectType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=80, verbose_name='Вид эффекта')),
            ],
            options={
                'verbose_name_plural': 'Виды эффектов от талантов',
                'verbose_name': 'Вид эффекта от таланта',
                'db_table': 'Power_effect_types',
            },
        ),
        migrations.CreateModel(
            name='Powers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=80, verbose_name='Название умения')),
                ('level', models.SmallIntegerField(verbose_name='Уровень')),
                ('value', models.SmallIntegerField(help_text="Например, 'Зональная вспышка 2'", null=True, verbose_name='Значение для типа действия или дальность атаки')),
                ('limit_range', models.SmallIntegerField(help_text='Например вспышка 2 в пределах 6', null=True, verbose_name='Значение в пределе')),
                ('hit_by_bonus', models.SmallIntegerField(null=True, verbose_name='Бонус к попаданию')),
                ('miss', models.CharField(max_length=255, null=True, verbose_name='Промах')),
                ('effects', models.TextField(help_text="Перечислите эффекты таланта через ';'", null=True, verbose_name='Результат успешно примененного таланта')),
                ('sustain', models.CharField(max_length=50, null=True, verbose_name='Поддержание эффекта')),
                ('special', models.TextField(null=True, verbose_name='Особенность использования')),
                ('accessories', models.ManyToManyField(to='powers.PowerAccessory', verbose_name='Приспособления')),
                ('action_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='powers.ActionType', verbose_name='Тип действия')),
                ('attack_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='powers.AttackType', verbose_name='Тип атаки')),
                ('class_assign', models.ManyToManyField(to='classes.Class', verbose_name='Умение класса')),
                ('damage_type', models.ManyToManyField(to='powers.PowerDamageType', verbose_name='Тип урона')),
                ('defense_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ability_defense_by', to='character.Ability', verbose_name='Характеристика защиты')),
                ('effect_type', models.ManyToManyField(to='powers.PowerEffectType', verbose_name='Тип эффекта')),
                ('hit_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ability_hit_by', to='character.Ability', verbose_name='Характеристика атаки')),
                ('paragon_paths', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='character.ParagonPath', verbose_name='Требуется путь предназначения')),
            ],
            options={
                'verbose_name_plural': 'Умения',
                'verbose_name': 'Умение',
                'db_table': 'Powers',
            },
        ),
        migrations.CreateModel(
            name='PowerUsageAbility',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=80, verbose_name='Возможность использования таланта')),
            ],
            options={
                'verbose_name_plural': 'Возможности использования талантов',
                'verbose_name': 'Возможность использования таланта',
                'db_table': 'Power_usage_ability',
            },
        ),
        migrations.CreateModel(
            name='Skills',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30, verbose_name='Название навыка')),
                ('key_ability', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='character.Ability')),
            ],
            options={
                'verbose_name_plural': 'Навыки',
                'verbose_name': 'Навык',
                'db_table': 'Skills',
            },
        ),
        migrations.CreateModel(
            name='Target',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=60, verbose_name='Наименование цели')),
            ],
            options={
                'verbose_name_plural': 'Цели',
                'verbose_name': 'Цель',
                'db_table': 'Targets',
            },
        ),
        migrations.AddField(
            model_name='powers',
            name='secondary_target',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='secondary_target', to='powers.Target', verbose_name='Цель'),
        ),
        migrations.AddField(
            model_name='powers',
            name='target',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='powers.Target', verbose_name='Цель'),
        ),
        migrations.AddField(
            model_name='powers',
            name='tranning_skills_require',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='powers.Skills', verbose_name='Требуется тренировка навыка'),
        ),
        migrations.AddField(
            model_name='powers',
            name='usage_ability',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='powers.PowerUsageAbility', verbose_name='Возможность использования'),
        ),
    ]

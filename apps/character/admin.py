from django.contrib import admin
from .models import Character, EpicDestiny, ParagonPath, Ability, DefenseType

for model in (Character, EpicDestiny, ParagonPath, Ability, DefenseType):
    admin.site.register(model)

from django.db import models
from apps.classes.models import Class


class DefenseType(models.Model):
    class Meta:
        verbose_name = "Защита"
        verbose_name_plural = "Защиты"
        db_table = "Defenses"

    name = models.CharField(verbose_name="Наименование защиты", max_length=30, null=False, blank=False)
    short_name = models.CharField(verbose_name="Наименование защиты (кратко)", max_length=6, null=False, blank=False)

    def __str__(self):
        return "{}[{}]".format(self.name, self.short_name)


class Character(models.Model):
    class Meta:
        verbose_name = "Персонаж"
        verbose_name_plural = "Персонажи"
        db_table = "Characters"

    name = models.CharField(verbose_name="Имя персонажа", max_length=80)
    character_class = models.CharField(verbose_name="Класс", max_length=50)
    paragon_path = models.CharField(verbose_name="Путь совершенства", max_length=255)
    epic_destiny = models.CharField(verbose_name="Эпическое предназначение", max_length=50)
    race = models.CharField(verbose_name="Раса", max_length=30)
    size = models.CharField(verbose_name="Размер", max_length=10)
    age = models.SmallIntegerField(verbose_name="Возраст")
    gender = models.CharField(verbose_name="Пол", max_length=10)
    height = models.CharField(verbose_name="Рост", max_length=4)
    weight = models.CharField(verbose_name="Вес", max_length=4)
    alignment = models.CharField(verbose_name="Мировоззрение", max_length=30)
    deity = models.CharField(verbose_name="Вера", max_length=30)
    adventure_team = models.CharField(verbose_name="Команда или другая организация", max_length=100)

    # ABILITY SCORES
    base_initiative = models.SmallIntegerField(verbose_name="Базовая инициатива", default=0)
    base_strength = models.SmallIntegerField(verbose_name="Базовая сила", default=0)
    base_constitution = models.SmallIntegerField(verbose_name="Базовое телосложение", default=0)
    base_dexterity = models.SmallIntegerField(verbose_name="Базовая ловкость", default=0)
    base_intelligence = models.SmallIntegerField(verbose_name="Базовый интеллект", default=0)
    base_wisdom = models.SmallIntegerField(verbose_name="Базовая мудрость", default=0)
    base_charisma = models.SmallIntegerField(verbose_name="Базовая харизма", default=0)

    # DEFENSES
    base_ac = models.SmallIntegerField(verbose_name="Базовый класс доспеха", default=10)
    base_fort = models.SmallIntegerField(verbose_name="Базовая стойкость", default=10)
    base_ref = models.SmallIntegerField(verbose_name="Базовая реакция", default=10)
    base_will = models.SmallIntegerField(verbose_name="Базовая воля", default=10)
    resistance_fire = models.SmallIntegerField(verbose_name="Базовая сопротивляемость огню", default=0)
    resistance_cold = models.SmallIntegerField(verbose_name="Базовая сопротивляемость холоду", default=0)
    resistance_dark = models.SmallIntegerField(verbose_name="Базовая сопротивляемость тьме", default=0)
    resistance_light = models.SmallIntegerField(verbose_name="Базовая сопротивляемость свету", default=0)
    resistance_shock = models.SmallIntegerField(verbose_name="Базовая сопротивляемость шоку", default=0)

    hp = models.IntegerField(verbose_name="Хиты")
    healing_surges_value = models.SmallIntegerField(verbose_name="Значение исцеления")
    healing_surges_count_per_day = models.SmallIntegerField(verbose_name="Кол-во исцелений в день")
    temporary_hp = models.SmallIntegerField(verbose_name="Временные хиты", default=0)

    base_speed = models.SmallIntegerField(verbose_name="Базовая скорость")


class Ability(models.Model):
    class Meta:
        verbose_name = "Характеристика"
        verbose_name_plural = "Характеристики"
        db_table = "Abilities"

    name = models.CharField(verbose_name="Название аттрибута", max_length=80)
    short_name = models.CharField(verbose_name="Сокращенное название аттрибута", max_length=4, null=False, blank=False)
    description = models.TextField(verbose_name="Описание аттрибута")

    @property
    def modifier(self, ability_value):
        return int(ability_value / 2) - 5

    def __str__(self):
        return "{}[{}]".format(self.name, self.short_name)


class ParagonPath(models.Model):
    class Meta:
        verbose_name = "Путь совершенства"
        verbose_name_plural = "Пути совершенства"
        db_table = "Paragon_paths"

    name = models.CharField(verbose_name="Название", max_length=80)
    character_class = models.ForeignKey(Class, verbose_name="Класс персонажа")


class EpicDestiny(models.Model):
    class Meta:
        verbose_name = "Эпическое предназначение"
        verbose_name_plural = "Эпические предназначения"
        db_table = "Epic_destinies"

    name = models.CharField(verbose_name="Название", max_length=80)

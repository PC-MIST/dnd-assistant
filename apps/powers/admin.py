from django.contrib import admin
from .models import classes_list

for model in classes_list:
    admin.site.register(model)

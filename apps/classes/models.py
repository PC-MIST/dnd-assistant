from django.db import models


class PowerSource(models.Model):
    class Meta:
        verbose_name = "Источник силы"
        verbose_name_plural = "Источники силы"
        db_table = "Power_sources"

    name = models.CharField(verbose_name="Источник силы", max_length=80)
    description = models.TextField(verbose_name="Описание")

    def __str__(self):
        return self.name


class Class(models.Model):
    class Meta:
        verbose_name = "Класс"
        verbose_name_plural = "Классы"
        db_table = "Classes"

    name = models.CharField(verbose_name="Название класса", max_length=80)
    power_source = models.ForeignKey(PowerSource, verbose_name="Источник силы")

    def __str__(self):
        return self.name


class ClassBenefit(models.Model):
    class Meta:
        verbose_name = "Классовое преимущество"
        verbose_name_plural = "Классовые преимущества"
        db_table = "Class_benefits"

    name = models.CharField(verbose_name="Название преимущества", max_length=40)
    description = models.TextField(verbose_name="Описание преимущества")
    character_class = models.ForeignKey(Class, verbose_name="Преимущество для класса")

    def __str__(self):
        return "{} ({})".format(self.name, self.character_class)

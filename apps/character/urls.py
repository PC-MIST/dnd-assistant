from django.conf.urls import url

from .views import Character

urlpatterns = [
    url(r'^$', Character.as_view(), name='index'),
]

from django.db import models
from apps.classes.models import Class
from apps.character.models import Ability, ParagonPath, EpicDestiny, DefenseType


class PowerUsageAbility(models.Model):
    class Meta:
        verbose_name = "Возможность использования таланта"
        verbose_name_plural = "Возможности использования талантов"
        db_table = "Power_usage_ability"

    name = models.CharField(verbose_name="Возможность использования таланта", max_length=80)
    description = models.TextField(verbose_name="Описание", null=False, blank=False, default="")

    def __str__(self):
        return "{}: {}...".format(self.name, self.description[:60])


class PowerDamageType(models.Model):
    class Meta:
        verbose_name = "Вид урона"
        verbose_name_plural = "Виды урона"
        db_table = "Power_damage_types"

    name = models.CharField(verbose_name="Тип урона", max_length=80)
    description = models.TextField(verbose_name="Описание", null=False, blank=False, default="")

    def __str__(self):
        return "{}: {}".format(self.name, self.description)


class PowerEffectType(models.Model):
    class Meta:
        verbose_name = "Вид эффекта от таланта"
        verbose_name_plural = "Виды эффектов от талантов"
        db_table = "Power_effect_types"

    name = models.CharField(verbose_name="Вид эффекта", max_length=80)
    description = models.TextField(verbose_name="Описание", null=False, blank=False, default="")

    def __str__(self):
        return "{}: {}".format(self.name, self.description[:40])


class PowerAccessory(models.Model):
    class Meta:
        verbose_name = "Приспособление для использования таланта"
        verbose_name_plural = "Приспособления для использования таланта"
        db_table = "Power_accessories"

    name = models.CharField(verbose_name="Тип приспособления", max_length=80)
    description = models.TextField(verbose_name="Описание", null=False, blank=False, default="")

    def __str__(self):
        return self.name


class ActionType(models.Model):
    class Meta:
        verbose_name = "Тип действия"
        verbose_name_plural = "Типы действий"
        db_table = "Action_types"

    name = models.CharField(verbose_name="Тип действия", max_length=80)
    description = models.TextField(verbose_name="Описание", null=False, blank=False, default="")

    def __str__(self):
        return self.name

        # Рукопашный
        # Дальнобойный
        # Ближний
        # Зональный
        # Персональный


class AttackType(models.Model):
    class Meta:
        verbose_name = "Тип атаки"
        verbose_name_plural = "Типы атак"
        db_table = "Attack_types"

    name = models.CharField(verbose_name="Тип атаки", max_length=80)
    description = models.TextField(verbose_name="Описание", null=True, blank=False, default="")

    def __str__(self):
        return self.name


class Target(models.Model):
    class Meta:
        verbose_name = "Цель"
        verbose_name_plural = "Цели"
        db_table = "Targets"

    name = models.CharField(verbose_name="Наименование цели", max_length=60)

    def __str__(self):
        return self.name


class Skills(models.Model):
    class Meta:
        verbose_name = "Навык"
        verbose_name_plural = "Навыки"
        db_table = "Skills"

    name = models.CharField(verbose_name="Название навыка", max_length=30)
    key_ability = models.ForeignKey(Ability, verbose_name="Ключевой навык")
    armor_penalty = models.BooleanField(verbose_name="Штраф проверки за доспех", default=False)
    description = models.TextField(verbose_name="Описание", null=False, blank=False, default="")

    def __str__(self):
        return "{}[{}]".format(self.name, self.key_ability.short_name)


class Powers(models.Model):
    class Meta:
        verbose_name = "Умение"
        verbose_name_plural = "Умения"
        db_table = "Powers"

    name = models.CharField(verbose_name="Название умения", max_length=80, blank=True)
    class_assign = models.ForeignKey(Class, verbose_name="Умение класса", null=True, blank=True)
    level = models.SmallIntegerField(verbose_name="Уровень", blank=True)
    description = models.TextField(verbose_name="Описание", default="", null=False, blank=True)

    # KEY WORDS
    usage_ability = models.ForeignKey(PowerUsageAbility, verbose_name="Возможность использования", blank=True)
    damage_type = models.ManyToManyField(PowerDamageType, verbose_name="Тип урона", blank=True)
    effect_type = models.ManyToManyField(PowerEffectType, verbose_name="Тип эффекта", blank=True)
    accessories = models.ManyToManyField(PowerAccessory, verbose_name="Приспособления", blank=True)

    # ATTACKS, LIMITS, RANGES, TARGETS
    action_type = models.ForeignKey(ActionType, verbose_name="Тип действия", blank=True)
    attack_type = models.ForeignKey(AttackType, verbose_name="Тип атаки", blank=True)
    value = models.SmallIntegerField(verbose_name="Значение для типа действия или дальность атаки",
                                     help_text="Например, 'Зональная вспышка [2]'", null=True, blank=True)
    limit_range = models.SmallIntegerField(verbose_name="Значение в пределе",
                                           help_text="Например вспышка 2 в пределах [6]", null=True, blank=True)
    hit_by = models.ForeignKey(Ability, verbose_name="Характеристика атаки", related_name='ability_hit_by', null=True,
                               blank=True)
    hit_by_bonus = models.SmallIntegerField(verbose_name="Бонус к попаданию", null=True, blank=True)
    defense_by = models.ForeignKey(DefenseType, verbose_name="Характеристика защиты", related_name='ability_defense_by',
                                   null=True, blank=True)
    target = models.ForeignKey(Target, verbose_name="Цель")
    secondary_target = models.ForeignKey(Target, verbose_name="Вторичная цель", related_name='secondary_target',
                                         null=True, blank=True)
    miss = models.CharField(verbose_name="Промах", max_length=255, null=True, blank=True)

    # REQUIREMENTS
    tranning_skills_require = models.ForeignKey(Skills, verbose_name="Требуется тренировка навыка", null=True,
                                                blank=True)
    paragon_paths = models.ForeignKey(ParagonPath, verbose_name="Требуется путь предназначения", null=True, blank=True)

    # EFFECTS
    effects = models.TextField(verbose_name="Результат успешно примененного таланта",
                               help_text="Перечислите эффекты таланта через ';'", null=True, blank=True)
    sustain = models.CharField(verbose_name="Поддержание эффекта", max_length=50, null=True, blank=True)

    # MISC
    special = models.TextField(verbose_name="Особенность использования", null=True, blank=True)

    def __str__(self):
        return "Умение: {}. Класс: {}. Уровень: {}".format(self.name, self.class_assign.name, self.level)


classes_list = [Powers, Skills, Target, AttackType, ActionType, PowerAccessory, PowerEffectType, PowerDamageType,
                PowerUsageAbility]
